package com.dain_torson.fileselector.view.fileselector;


import javafx.scene.control.TreeItem;

import java.io.File;

public class FolderItem extends TreeItem<String>{

    private File file;
    private String name;

    public FolderItem(File file) {
        super();
        String path = file.getAbsolutePath();
        int index = path.lastIndexOf('\\');
        if(index != path.length() - 1) {
            name = path.substring(index + 1, path.length());
        }
        else {
            name = path;
        }
        setValue(name);
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
