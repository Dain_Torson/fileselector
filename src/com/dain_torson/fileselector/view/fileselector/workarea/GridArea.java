package com.dain_torson.fileselector.view.fileselector.workarea;


import com.dain_torson.fileselector.controller.filesselector.events.FileEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import java.io.File;

public class GridArea extends GridPane {

    private int elementsPerRow = 2;

    public GridArea(File[] files) {;
        initGrid(files);
    }

    private GridElement initElement(File file, int colIdx, int rowIdx) {

        GridElement element = new GridElement(file);
        element.setOnMouseClicked(new ElementClickedHandler());
        GridPane.setColumnIndex(element, colIdx);
        GridPane.setRowIndex(element, rowIdx);
        GridPane.setMargin(element, new Insets(10, 20, 10, 20));


        return element;
    }

    private void initGrid(File [] files) {
        this.getChildren().clear();
        int colIdx = 0;
        int rowIdx = 0;

        for(File file : files) {
            if(file.isDirectory()) {
                this.getChildren().add(initElement(file, colIdx, rowIdx));
                if (colIdx > elementsPerRow * 4) {
                    colIdx = 0;
                    rowIdx += 1;
                } else {
                    colIdx += 4;
                }
            }
        }

        for(File file : files) {
            if(file.isFile()) {
                this.getChildren().add(initElement(file, colIdx, rowIdx));
                if (colIdx > elementsPerRow * 4) {
                    colIdx = 0;
                    rowIdx += 1;
                } else {
                    colIdx += 4;
                }
            }
        }
    }

    public void setFiles(File[] files) {
        initGrid(files);
    }

    private class ElementClickedHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {

            GridElement element = (GridElement) event.getSource();
            fireEvent(new FileEvent(FileEvent.FILE_SELECTED, element.getFile()));
        }
    }

}
