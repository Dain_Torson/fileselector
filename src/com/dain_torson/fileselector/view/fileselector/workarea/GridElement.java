package com.dain_torson.fileselector.view.fileselector.workarea;


import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

import java.io.File;

public class GridElement extends VBox{

    private File file;
    private String name;
    private Label label;
    private Rectangle rectangle;

    public GridElement(File file) {
        this.file = file;
        rectangle = new Rectangle(100, 50);
        label = new Label();

        if(file.isDirectory()) {
            String path = file.getAbsolutePath();
            int index = path.lastIndexOf('\\');
            if(index != path.length() - 1) {
                name = path.substring(index + 1, path.length());
            }
        }
        else {
            name = file.getName();
        }

        setMaxWidth(100);
        label.setText(name);
        getChildren().addAll(rectangle, label);
        setAlignment(Pos.CENTER);
    }

    public File getFile() {
        return file;
    }
}
