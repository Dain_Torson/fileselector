package com.dain_torson.fileselector.view.fileselector.workarea;

import com.dain_torson.fileselector.controller.filesselector.events.FileEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.io.File;

public class ListArea extends VBox {

    public ListArea(File[] files) {
        initList(files);
    }

    private void initList(File[] files) {
        for(File file : files) {
            if(file.isDirectory()) {
                ListElement element = new ListElement(file);
                element.setOnMouseClicked(new ElementClickedHandler());
                this.getChildren().add(element);
            }
        }

        for(File file : files) {
            if(file.isFile()) {
                ListElement element = new ListElement(file);
                element.setOnMouseClicked(new ElementClickedHandler());
                this.getChildren().add(element);
            }
        }
    }

    public void setFiles(File [] files) {
        initList(files);
    }

    private class ElementClickedHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {

            ListElement element = (ListElement) event.getSource();
            fireEvent(new FileEvent(FileEvent.FILE_SELECTED, element.getFile()));
        }
    }
}
