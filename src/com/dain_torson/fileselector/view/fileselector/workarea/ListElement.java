package com.dain_torson.fileselector.view.fileselector.workarea;


import javafx.scene.control.Label;

import java.io.File;

public class ListElement extends Label {

    private File file;
    private String value;

    public ListElement(File file) {
        this.file = file;

        if(file.isDirectory()) {
            String path = file.getAbsolutePath();
            int index = path.lastIndexOf('\\');
            if(index != path.length() - 1) {
                value = path.substring(index + 1, path.length());
            }
        }
        else {
        value = file.getName();
    }

        this.setText(value);
    }

    public File getFile() {
        return file;
    }
}
