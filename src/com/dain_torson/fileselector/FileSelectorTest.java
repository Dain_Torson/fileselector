package com.dain_torson.fileselector;

import com.dain_torson.fileselector.controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FileSelectorTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("view/view.fxml").openStream());
        primaryStage.setTitle("FileSelector test");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 400, 300));
        Controller controller = fxmlLoader.getController();
        controller.setStage(primaryStage);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}