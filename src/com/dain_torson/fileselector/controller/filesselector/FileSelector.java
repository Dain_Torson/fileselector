package com.dain_torson.fileselector.controller.filesselector;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class FileSelector implements Initializable {

    private FolderAreaController folderAreaController;
    private WorkAreaController workAreaController;

    private static File targetFile;
    private WorkMode workMode;
    private static File homeFolder;
    private static String [] filters;
    private static Stage stage;
    @FXML
    private Pane folderArea;
    @FXML
    private Pane workArea;
    @FXML
    private ChoiceBox<String> viewChoiceBox;
    @FXML
    private TextField fileNameField;
    @FXML
    private ChoiceBox<String> filterChoiceBox;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    public FileSelector() {

    }

    public void showOpenFileDialog(Stage source) throws IOException{

        FXMLLoader loader = new FXMLLoader();
        Parent root =  loader.load(FileSelector.class.getResource("../view/fileselector/fsopendialogview.fxml").openStream());
        Scene scene = new Scene(root, 800, 400);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Open");
        workMode = WorkMode.OPEN;
        stage.initModality(Modality.NONE);
        stage.initOwner(source);
        stage.showAndWait();

    }

    public void showSaveFileDialog(Stage source) throws IOException{

        FXMLLoader loader = new FXMLLoader();
        Parent root =  loader.load(FileSelector.class.getResource("../../view/fileselector/fsopendialogview.fxml").openStream());
        Scene scene = new Scene(root, 800, 400);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Save");
        workMode = WorkMode.SAVE;
        stage.initModality(Modality.NONE);
        stage.initOwner(source);
        stage.showAndWait();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        folderArea.getChildren().clear();
        workArea.getChildren().clear();
        folderAreaController = new FolderAreaController(folderArea, homeFolder);
        workAreaController = new WorkAreaController(workArea, folderAreaController, fileNameField);
        viewChoiceBox.getItems().addAll("Folders", "List", "Table");
        viewChoiceBox.setValue(viewChoiceBox.getItems().get(0));
        viewChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ViewChoiceChangedListener());

        filterChoiceBox.getItems().add("all");
        filterChoiceBox.setValue(filterChoiceBox.getItems().get(0));
        if(filters != null) {
            for(String filter : filters) {
                filterChoiceBox.getItems().add(filter);
            }
        }
        filterChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new FilterChoiceChangedListener());

        okButton.setOnAction(new OkButtonPressedHandler());
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });

    }

    public void setHomeFolder(File folder) {
        homeFolder = folder;
    }

    public void setFilters(String [] value) {
        filters = value;
    }

    public File getTargetFile() {
        return targetFile;
    }

    public enum ViewType {FOLDERS, LIST, TABLE}

    private enum WorkMode {SAVE, OPEN}

    private class ViewChoiceChangedListener implements ChangeListener {

        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {

            ViewType viewType = ViewType.FOLDERS;
            if((Integer)newValue == 1) {
                viewType = ViewType.LIST;
            }
            else if((Integer)newValue == 2) {
                viewType = ViewType.TABLE;
            }

            workAreaController.setViewType(viewType);
        }
    }

    private class FilterChoiceChangedListener implements ChangeListener {

        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {

            String filter = null;
            if((Integer)newValue != 0) {
                filter = filterChoiceBox.getItems().get((Integer)newValue);
            }
            workAreaController.setFilter(filter);
        }
    }

    private class OkButtonPressedHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            String fileName = fileNameField.getText();
            File folder = workAreaController.getCurrentFolder();

            if(folder != null) {
                targetFile = new File(folder.getAbsolutePath() + File.separator + fileName);
                if (workMode == WorkMode.SAVE) {
                    try {
                        if(targetFile.createNewFile()){
                            System.out.println("File");
                        }
                    }
                    catch (IOException exception) {
                        System.out.println(exception.getMessage());
                    }
                }
            }
            stage.close();
        }
    }
}
