package com.dain_torson.fileselector.controller.filesselector.workarea;


import com.dain_torson.fileselector.view.fileselector.workarea.GridArea;
import javafx.scene.Node;

import java.io.File;

public class GridAreaController implements ViewAreaController{

    private GridArea gridArea;
    private File[] files;

    public GridAreaController(File[] files) {
        this.files = files;
        gridArea = new GridArea(files);
    }

    public void setFiles(File[] files) {
        this.files = files;
        gridArea.setFiles(files);
    }

    @Override
    public Node getViewArea() {
        return gridArea;
    }

}
