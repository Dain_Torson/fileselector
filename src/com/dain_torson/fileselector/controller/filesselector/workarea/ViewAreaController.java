package com.dain_torson.fileselector.controller.filesselector.workarea;

import javafx.scene.Node;

import java.io.File;

public interface ViewAreaController {

    public void setFiles(File[] files);

    public Node getViewArea();
}
