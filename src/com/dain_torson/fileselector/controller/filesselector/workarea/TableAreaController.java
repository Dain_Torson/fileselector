package com.dain_torson.fileselector.controller.filesselector.workarea;


import com.dain_torson.fileselector.view.fileselector.workarea.TableArea;
import javafx.scene.Node;

import java.io.File;

public class TableAreaController implements ViewAreaController{

    private File [] files;
    private TableArea tableArea;

    public TableAreaController(File [] files) {
        this.files = files;
        tableArea = new TableArea(files);
    }

    @Override
    public void setFiles(File[] files) {
        this.files = files;
        tableArea.setFiles(files);
    }

    @Override
    public Node getViewArea() {
        return tableArea;
    }
}
