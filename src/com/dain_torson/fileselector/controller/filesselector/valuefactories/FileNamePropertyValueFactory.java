package com.dain_torson.fileselector.controller.filesselector.valuefactories;


import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.io.File;

public class FileNamePropertyValueFactory implements Callback<TableColumn.CellDataFeatures<File, String>, ObservableValue<String>>{

    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<File, String> param) {
        final File file = param.getValue();

        return new ObservableValue<String>() {
            @Override
            public void addListener(ChangeListener<? super String> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super String> listener) {

            }

            @Override
            public String getValue() {
                if(file.isDirectory()) {
                    return file.getName();
                }
                else {
                    String name = file.getName();
                    int index = name.lastIndexOf(".");
                    if(index != -1) {
                        name = name.substring(0, index);
                    }
                    return name;
                }
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }
}
