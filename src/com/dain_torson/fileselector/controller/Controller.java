package com.dain_torson.fileselector.controller;


import com.dain_torson.fileselector.controller.filesselector.FileSelector;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class Controller {

    private FileSelector fileSelector = new FileSelector();
    private Stage stage;

    @FXML
    private Button showSelectorButton;

    @FXML
    private void initialize()
    {

    }

    public void setStage(Stage stage) {
        this.stage = stage;
        showSelectorButton.setOnAction(new ShowButtonHandler(stage));
    }

    private class ShowButtonHandler implements EventHandler<ActionEvent> {

        private Stage source;

        public ShowButtonHandler(Stage source){
            this.source = source;
        }

        @Override
        public void handle(ActionEvent event) {

            FileChooser chooser = new FileChooser();
            try {
                File file = new File("E:" + File.separator + "Studying");
                fileSelector.setHomeFolder(file);
                fileSelector.setFilters(new String[]{".pdf", ".txt"});
                fileSelector.showSaveFileDialog(source);

            }
            catch (IOException exception) {
                exception.printStackTrace();
            }

            File file = fileSelector.getTargetFile();
            if(file != null) {
                System.out.println(file);
            }
        }
    }
}
